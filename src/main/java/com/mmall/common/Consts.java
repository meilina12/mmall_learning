package com.mmall.common;

import com.google.common.collect.Sets;
import java.util.Set;

/**
 * @author Mei
 * @since 2019—01-15
 */
public class Consts {

    //session域中当前对象的key
    public static final String CURRENT_USER="currentUser";

    //校验用户名 密码 的类型
    public static final String USERNAME="username";
    public static final String EMAIL="email";

    //token的前缀   guava的key的前缀
    public static final String TOKEN_PREFIX="token_";
    //ftp 前缀  与ftp的properties中对应
    public static final String ftp_server_http_prefix = "ftp.server.http.prefix";
    public static final String ftp_server_http_prefix_defaultValue = "http://img.happymmall.com/";

    public interface ProductListOrderBy {

        Set<String> PRICE_ASC_DESC = Sets.newHashSet("price_desc", "price_asc");

    }

    public enum ProductStatusEnum {
        on_Sale("在售", 1);
        private String value;
        private int code;

        ProductStatusEnum(String value, int code) {
            this.value = value;
            this.code = code;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }

    public interface Cart {

        int check = 1;//已经勾选
        int uncheck = 0;//未勾选
        String LIMIT_NUM_SUCCESS = "限制数据成功";
        String LIMIT_NUM_FAIL = "限制数据失败";

    }



    public interface Role{

        int ROLE_CUSTOMER = 1;//普通用户
        int ROLE_ADMIN = 0;//管理员

    }

    public enum OrderStatusEnum {
        CANCELED(0, "已取消"),
        NO_PAY(10, "未支付"),
        PAID(20, "已付款"),
        SHIPPED(40, "已发货"),
        ORDER_SUCCESS(50, "订单完成"),
        ORDER_CLOSE(60, "订单关闭");
        private int code;
        private String value;

        OrderStatusEnum(int code, String value) {
            this.code = code;
            this.value = value;
        }

        public int getCode() {
            return code;
        }

        public String getValue() {
            return value;
        }

        public static OrderStatusEnum codeOf(int code) {
            for (OrderStatusEnum orderStatusEnum : values()) {
                if (orderStatusEnum.getCode() == code) {
                    return orderStatusEnum;
                }
            }
            throw new RuntimeException("没有订单状态");
        }
    }

    public interface AlipayCallback {

        String TRADE_STATUS_WAIT_BUYER_PAY = "WAIT_BUYER_PAY";
        String TRADE_STATUS_TRADE_SUCCESS = "TRADE_SUCCESS";

        String RESPONSE_SUCCESS = "success";
        String RESPONSE_FAILED = "failed";

    }

    public enum PayPlatformEnum {
        ALIPAY(1, "支付宝");

        PayPlatformEnum(int code, String value) {
            this.code = code;
            this.value = value;
        }

        private String value;
        private int code;

        public String getValue() {
            return value;
        }

        public int getCode() {
            return code;
        }
    }

    public enum PaymentTypeEnum {
        online_PAY(1, "在线支付");

        PaymentTypeEnum(int code, String value) {
            this.code = code;
            this.value = value;
        }

        private String value;
        private int code;

        public String getValue() {
            return value;
        }

        public int getCode() {
            return code;
        }

        public static PaymentTypeEnum codeOf(int code) {
            for (PaymentTypeEnum paymentTypeEnum : values()) {
                if (paymentTypeEnum.getCode() == code) {
                    return paymentTypeEnum;
                }
            }
            throw new RuntimeException("没有找到对应的枚举");
        }
    }


}
