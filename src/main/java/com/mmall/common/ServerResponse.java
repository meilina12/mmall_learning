package com.mmall.common;

import java.io.Serializable;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 * @author Mei
 * @since 2019—01-14
 */
@JsonSerialize(include = Inclusion.NON_NULL)
public class ServerResponse<T> implements Serializable {

    private int status;
    private String message;
    private T data;

    private ServerResponse(int status){
        this.status=status;
    }

    private ServerResponse(int status,String message){
        this.status=status;
        this.message=message;
    }

    private ServerResponse(int status,T data){
        this.status=status;
        this.data=data;
    }

    private ServerResponse(int status,String message,T data){
        this.status=status;
        this.message=message;
        this.data=data;
    }

    public int getStatus(){
        return status;
    }

    public String getMessage(){
        return message;
    }

    public T getData(){
        return data;
    }
    @JsonIgnore
    public boolean isSuccess(){
        return status==ResponseCode.SUCCESS.getCode();
    }

    public static <T> ServerResponse<T> createBySuccess(){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode());
    }
    public static <T> ServerResponse<T> createBySuccessMessage(String message){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),message);
    }
    public static <T> ServerResponse<T> createBySuccess(T data){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),data);
    }
    public static <T> ServerResponse<T> createBySuccess(String message,T data){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),message,data);
    }

    public static <T> ServerResponse<T> createByERROR(){
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
    }
    public static <T> ServerResponse<T> createByERRORMessage(String errormessage){
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),errormessage);
    }
    public static <T> ServerResponse<T> createByERROR(int code,String message){
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),message);
    }



}
