package com.mmall.common;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mei
 * @since 2019—01-15
 */
public class TokenCache {

    private static Logger logger= LoggerFactory.getLogger(TokenCache.class);

    private static LoadingCache<String,String> localCache =CacheBuilder
                                .newBuilder()
                                .initialCapacity(1000)
                                .maximumSize(10000)
                                .expireAfterAccess(12, TimeUnit.HOURS)
                                .build(new CacheLoader<String, String>(){
                                    @Override
                                    public String load(String s) throws Exception{
                                        return null;
                                    }
                                });
    public static void setKey(String key,String value){
        localCache.put(key,value);
    }
    public static String getKey(String key){
        String value = null;
        try {
            value = localCache.get(key);
        } catch (ExecutionException e) {
            logger.error("localCache get error",e);
            e.printStackTrace();
        }
        if("null".equals(value)){
            return null;
        }
        return value;
    }

}
