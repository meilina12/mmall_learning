package com.mmall.controller.backend;

import com.mmall.common.Consts;
import com.mmall.common.Consts.Role;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;
import com.mmall.service.IUserService;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Mei
 * @since 2019—01-15
 */
@Controller
@RequestMapping("/manage/user/")
public class AdminController {

    @Autowired
    private IUserService iUserService;

    @RequestMapping(value = "login.do",method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> login(String username,String password, HttpSession session){
        ServerResponse<User> response = iUserService.login(username, password);
        if(response.isSuccess()){
            User user=response.getData();
            session.setAttribute(Consts.CURRENT_USER,user);
            if(user.getRole().equals(Role.ROLE_ADMIN)){
                return ServerResponse.createBySuccess(user);
            }
            return ServerResponse.createByERRORMessage("不是管理员登录");
        }
        return response;
    }

}
