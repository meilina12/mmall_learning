package com.mmall.controller.backend;

import com.mmall.common.ServerResponse;
import com.mmall.service.ICategoryService;
import com.mmall.service.IUserService;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Mei
 * @since 2019—01-16
 */
@Controller
@RequestMapping("/manage/category/")
public class CategoryController {

    @Autowired
    private IUserService iUserService;

    @Autowired
    private ICategoryService icategoryService;

    @RequestMapping("add_category.do")
    @ResponseBody
    public ServerResponse addCategory(HttpSession session, String categoryName, @RequestParam(value = "parentId", defaultValue = "0") int parentId) {
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            return icategoryService.addCategory(categoryName, parentId);
        }
        return serverResponse;
    }

    @RequestMapping("update_category.do")
    @ResponseBody
    public ServerResponse updateCategory(HttpSession session, int categoryId, String categoryName) {
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            //更新操作
            return icategoryService.updateCategory(categoryName, categoryId);
        }
        return serverResponse;
    }

    @RequestMapping("get_category.do")
    @ResponseBody
    public ServerResponse getChildParallelCategory(HttpSession session, @RequestParam(value = "categoryId", defaultValue = "0") int parentId) {
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            //查询
            return icategoryService.getChildParaller(parentId);
        }
        return serverResponse;
    }

    /**
     * 查询id及孩子节点
     */
    @RequestMapping("get_deep_category.do")
    @ResponseBody
    public ServerResponse getChildCategory(HttpSession session, @RequestParam(value = "categoryId", defaultValue = "0") int parentId) {
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            //查询及孩子节点
            return icategoryService.getChildCategory(parentId);
        }
        return serverResponse;
    }


}
