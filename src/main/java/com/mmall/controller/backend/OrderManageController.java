package com.mmall.controller.backend;

import com.github.pagehelper.PageInfo;
import com.mmall.common.ServerResponse;
import com.mmall.service.IOrderService;
import com.mmall.service.IUserService;
import com.mmall.vo.OrderVo;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Mei
 * @since 2019—02-13
 */
@Controller
@RequestMapping("/manage/order/")
public class OrderManageController {

    @Autowired
    private IUserService iUserService;

    @Autowired
    private IOrderService iOrderService;

    @RequestMapping("list.do")
    @ResponseBody
    public ServerResponse<PageInfo> orderList(HttpSession session,
        @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
        @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        //判断是不是管理员登录
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            //填充我们增加产品的业务逻辑
            return iOrderService.manageList(pageNum, pageSize);
        }
        return serverResponse;
    }

    @RequestMapping("detail.do")
    @ResponseBody
    public ServerResponse<OrderVo> orderDetail(HttpSession session, Long orderNo) {
        //判断是不是管理员登录
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            //填充我们增加产品的业务逻辑
            return iOrderService.manageDetail(orderNo);
        }
        return serverResponse;
    }

    @RequestMapping("search.do")
    @ResponseBody
    public ServerResponse<PageInfo> orderSearch(HttpSession session, Long orderNo, @RequestParam(value = "pageNum", defaultValue = "1") int pageNum,
        @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {
        //判断是不是管理员登录
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            return iOrderService.manageSearch(orderNo, pageNum, pageSize);
        }
        return serverResponse;
    }

    @RequestMapping("send_goods.do")
    @ResponseBody
    public ServerResponse<String> orderSendGoods(HttpSession session, Long orderNo) {
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            return iOrderService.manageSendGoods(orderNo);
        }
        return serverResponse;
    }

}
