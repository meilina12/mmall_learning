package com.mmall.controller.backend;

import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Product;
import com.mmall.service.IFileService;
import com.mmall.service.IProductService;
import com.mmall.service.IUserService;
import com.mmall.util.PropertiesUtil;
import com.mmall.vo.ProductDetailVo;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Mei
 * @since 2019—01-18
 */
@Controller
@RequestMapping("/manage/product/")
public class ProduceController {

    @Autowired
    private IUserService iUserService;

    @Autowired
    private IProductService iproductService;

    @Autowired
    private IFileService iFileService;

    @RequestMapping("addProduct.do")
    @ResponseBody
    public ServerResponse addProduct(HttpSession session, Product product) {
        //判断是否为管理员
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            //添加商品逻辑
            return iproductService.saveOrUpdate(product);
        }
        return serverResponse;
    }

    @RequestMapping("set_Product_status.do")
    @ResponseBody
    public ServerResponse setProductStatus(HttpSession session, Integer productId, Integer status) {
        //判断是否为管理员
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            return iproductService.setProductStatus(productId, status);
        }
        return serverResponse;
    }

    /**
     * 获取产品详情
     */
    @RequestMapping("detail.do")
    @ResponseBody
    public ServerResponse<ProductDetailVo> getDetail(HttpSession session, Integer productId) {
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            return iproductService.manageProductDetail(productId);
        }
        return serverResponse;

    }

    /**
     * 后台获取产品分页
     */
    @RequestMapping("list.do")
    @ResponseBody
    public ServerResponse<PageInfo> getList(HttpSession session, @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum, @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            return iproductService.getProductList(pageNum, pageSize);
        }
        return serverResponse;
    }

    /**
     * 后台搜索产品
     */
    @RequestMapping("search.do")
    @ResponseBody
    public ServerResponse<PageInfo> productSearch(HttpSession session, String productName, Integer productId, @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            return iproductService.searchProduct(productName, productId, pageNum, pageSize);
        }
        return serverResponse;
    }

    /**
     * 上传文件  返回url
     */
    @RequestMapping("upload.do")
    @ResponseBody
    public ServerResponse upload(HttpSession session, @RequestParam(value = "upload_file", required = false) MultipartFile file, HttpServletRequest request) {
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            //得到upload文件夹的相对路径    发布后文件会创建到webapp/upload
            String path = request.getSession().getServletContext().getRealPath("upload");
            String targetFileName = iFileService.upload(file, path);
            String property = PropertiesUtil.getProperty("ftp.server.http.prefix");
            String url = new StringBuilder(property).append(targetFileName).toString();
            Map fileMap = Maps.newHashMap();
            fileMap.put("uri", targetFileName);
            fileMap.put("url", url);
            return ServerResponse.createBySuccess(fileMap);
        }
        return serverResponse;
    }

    /**
     * 富文本上传文件  返回url
     */
    @RequestMapping("richtext_img_upload.do")
    @ResponseBody
    public Map richtextImgUpload(HttpSession session, @RequestParam(value = "upload_file", required = false) MultipartFile file, HttpServletRequest request, HttpServletResponse response) {
        Map resultMap = Maps.newHashMap();
        ServerResponse serverResponse = iUserService.checkLoginAdmin(session);
        if (serverResponse.isSuccess()) {
            //得到upload文件夹的相对路径    发布后文件会创建到webapp/upload
            //富文本有自己的要求
            String path = request.getSession().getServletContext().getRealPath("upload");
            String targetFileName = iFileService.upload(file, path);
            if (StringUtils.isEmpty(targetFileName)) {
                resultMap.put("success", false);
                resultMap.put("msg", "上传文件失败");
                return resultMap;
            }
            String property = PropertiesUtil.getProperty("ftp.server.http.prefix");
            String url = new StringBuilder(property).append(targetFileName).toString();
            resultMap.put("success", true);
            resultMap.put("msg", "上传文件成功");
            resultMap.put("file_path", url);
            response.addHeader("Access-Control-Allow-Headers", "X-File-Name");
            return resultMap;
        }
        resultMap.put("success", false);
        resultMap.put("msg", "请登录管理员/无权限操作");
        return resultMap;
    }


}
