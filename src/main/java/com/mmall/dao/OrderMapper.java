package com.mmall.dao;

import com.mmall.pojo.Order;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);

    Order selectByOrderNoUserId(@Param("orderno") Long orderno, @Param("userId") Integer userId);

    Order selectByOrderNO(Long orderno);

    List<Order> selectByUserId(Integer userId);

    List<Order> selectAllOrder();


}