package com.mmall.dao;

import com.mmall.pojo.Product;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ProductMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Product record);

    int insertSelective(Product record);

    Product selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Product record);

    int updateByPrimaryKey(Product record);

    int updateProduceStatus(@Param("productId") Integer productId, @Param("status") Integer status);

    List<Product> selectList();

    List<Product> selectByNameId(@Param("productName") String productName, @Param("productId") Integer productId);

    List<Product> selectByNameAndCategoryIds(@Param("keyword") String keyword, @Param("productIdList") List<Integer> categoryIds);
}