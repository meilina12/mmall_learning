package com.mmall.dao;

import com.mmall.pojo.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    int checkUsername(String username);

    int checkEmail(String email);

    User selectLogin(@Param("username") String username,@Param("password") String password);

    String getQuestionByusername(String username);

    int checkAnswer(@Param("username") String username,@Param("question") String question,@Param("answer")String answer);

    int setPwdByusername(@Param("username") String username,@Param("newPwd") String newPwd);

    int checkPassword(@Param("id") Integer id, @Param("oldPwd") String oldPwd);

    int checkEmailByUseId(@Param("id")int id,@Param("email") String email);

}