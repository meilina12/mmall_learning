package com.mmall.service;

import com.mmall.common.ServerResponse;
import com.mmall.pojo.Category;
import java.util.List;

/**
 * @author Mei
 * @since 2019—01-16
 */
public interface ICategoryService {

    ServerResponse addCategory(String name,Integer parentId);

    ServerResponse updateCategory(String categoryName,Integer categoryId);

    ServerResponse<List<Category>> getChildParaller(Integer id);

    ServerResponse<List<Integer>> getChildCategory(Integer categoryId);

}
