package com.mmall.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Mei
 * @since 2019—01-24
 */
public interface IFileService {

    String upload(MultipartFile file, String path);

}
