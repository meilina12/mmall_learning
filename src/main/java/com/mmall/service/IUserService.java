package com.mmall.service;

import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;
import javax.servlet.http.HttpSession;

/**
 * @author Mei
 * @since 2019—01-14
 */
public interface IUserService {

     ServerResponse<User> login(String username,String password);

     ServerResponse<String> register(User user);

     ServerResponse<String> checkValid(String str,String type);

     ServerResponse<String> forgetGetQuestion(String username);

     ServerResponse<String> checkAnswer(String username,String question,String answer);

     ServerResponse<String> forgetSetNewPwd(String username,String newPwd,String forgetUUID);

     ServerResponse<String> resetPassword(User user,String oldPwd,String newPwd);

     ServerResponse<User> updateUserInfo(User user);

     ServerResponse<User> getInformation(Integer id);

     ServerResponse checkAdmin(User user);

    ServerResponse checkLoginAdmin(HttpSession session);

}
