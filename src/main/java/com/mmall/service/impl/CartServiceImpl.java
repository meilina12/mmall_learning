package com.mmall.service.impl;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.mmall.common.Consts;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.dao.CartMapper;
import com.mmall.dao.ProductMapper;
import com.mmall.pojo.Cart;
import com.mmall.pojo.Product;
import com.mmall.service.ICartService;
import com.mmall.util.BigDecimalUtil;
import com.mmall.util.PropertiesUtil;
import com.mmall.vo.CartProductVo;
import com.mmall.vo.CartVo;
import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * @author Mei
 * @since 2019—01-29
 */
@Controller("iCartService")
public class CartServiceImpl implements ICartService {

    @Autowired
    private CartMapper cartMapper;

    @Autowired
    private ProductMapper productMapper;


    public ServerResponse<CartVo> add(Integer userId, Integer productId, Integer count) {
        if (productId == null || count == null) {
            return ServerResponse.createByERROR(ResponseCode.ILLEGAL_ARGUMENT.getCode(), ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        Cart cart = cartMapper.selectByIdAndProductId(userId, productId);
        if (cart == null) {
            cart = new Cart();
            cart.setUserId(userId);
            cart.setProductId(productId);
            cart.setChecked(Consts.Cart.check);//设置为选定状态
            cart.setQuantity(count);
            cartMapper.insert(cart);
        } else {
            //说明产品在购物车  （以前添加过的)
            cart.setQuantity(cart.getQuantity() + count);
            cartMapper.updateByPrimaryKeySelective(cart);
        }
        CartVo cartVoLimit = this.getCartVoLimit(userId);
        return ServerResponse.createBySuccess(cartVoLimit);
    }

    public ServerResponse<CartVo> update(Integer userId, Integer productId, Integer count) {
        if (productId == null || count == null) {
            return ServerResponse.createByERROR(ResponseCode.ILLEGAL_ARGUMENT.getCode(), ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        Cart cart = cartMapper.selectByIdAndProductId(userId, productId);
        if (cart != null) {
            cart.setQuantity(count);
        }
        cartMapper.updateByPrimaryKeySelective(cart);
        return ServerResponse.createBySuccess(this.getCartVoLimit(userId));
    }

    public ServerResponse delete(Integer userId, String productIds) {
        //将products分隔
        List<String> strings = Splitter.on(',').splitToList(productIds);
        if (CollectionUtils.isEmpty(strings)) {
            return ServerResponse.createByERROR(ResponseCode.ILLEGAL_ARGUMENT.getCode(), ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        cartMapper.deleteByUserIdAndProducts(userId, strings);
        return ServerResponse.createBySuccess(getCartVoLimit(userId));

    }

    @Override
    public ServerResponse<CartVo> list(Integer userId) {
        return ServerResponse.createBySuccess(this.getCartVoLimit(userId));
    }

    @Override
    public ServerResponse<CartVo> selectOrUnselect(Integer userId, Integer productId, Integer checked) {
        //更新选择的状态
        cartMapper.checkOrUncheckProduct(userId, productId, checked);
        //查询结果
        return this.list(userId);
    }

    public ServerResponse<Integer> getCartProductCount(Integer userId) {
        return ServerResponse.createBySuccess(cartMapper.selectProductCount(userId));
    }


    private CartVo getCartVoLimit(Integer userId) {
        CartVo cartVo = new CartVo();
        List<Cart> cartList = cartMapper.selectCartByUserId(userId);
        List<CartProductVo> cartProductVoList = Lists.newArrayList();
        //用BigDecimal 使用字符串的构造器  这样的精度不会错误
        BigDecimal cartTotalPrice = new BigDecimal("0");
        if (CollectionUtils.isNotEmpty(cartList)) {
            for (Cart cart : cartList) {
                CartProductVo cartProductVo = new CartProductVo();
                cartProductVo.setId(cart.getId());
                cartProductVo.setUserId(cart.getUserId());
                cartProductVo.setProductId(cart.getProductId());
                //根据productId查询产品
                Product product = productMapper.selectByPrimaryKey(cart.getProductId());
                if (product != null) {
                    //组装数据
                    cartProductVo.setProductName(product.getName());
                    cartProductVo.setProductMainImage(product.getMainImage());
                    cartProductVo.setProductSubTitle(product.getSubtitle());
                    cartProductVo.setProductStatus(product.getStatus());
                    cartProductVo.setProductPrice(product.getPrice());
                    cartProductVo.setProductStock(product.getStock());
                    //判断库存
                    int buylimitCount = 0;
                    if (product.getStock() >= cart.getQuantity()) {
                        //库存充足
                        buylimitCount = cart.getQuantity();
                        cartProductVo.setLimitQuantity(Consts.Cart.LIMIT_NUM_SUCCESS);
                    } else {
                        //库存不足
                        buylimitCount = product.getStock();
                        cartProductVo.setLimitQuantity(Consts.Cart.LIMIT_NUM_FAIL);
                        //更新数据
                        Cart cart1 = new Cart();
                        cart1.setId(cart.getId());
                        cart1.setQuantity(product.getStock());
                        cartMapper.updateByPrimaryKeySelective(cart1);
                    }
                    cartProductVo.setQuantity(buylimitCount);
                    //小计【每一个产品的小计】
                    BigDecimal mult = BigDecimalUtil.mult(cartProductVo.getProductPrice().doubleValue(), cartProductVo.getQuantity().doubleValue());
                    cartProductVo.setProductTotalPrice(mult);
                    //进行勾选
                    cartProductVo.setProductChecked(cart.getChecked());
                }
                //计算总价
                if (cart.getChecked() == Consts.Cart.check) {
                    cartTotalPrice = BigDecimalUtil.add(cartTotalPrice.doubleValue(), cartProductVo.getProductTotalPrice().doubleValue());
                }
                cartProductVoList.add(cartProductVo);
            }
            cartVo.setCartTotalPrice(cartTotalPrice);
            cartVo.setCartProductVoList(cartProductVoList);
            cartVo.setAllChecked(getAllCheckedStatus(userId));
            cartVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix"));
        }
        return cartVo;
    }

    //判断是否为全选
    public boolean getAllCheckedStatus(Integer userId) {
        if (userId == null) {
            return false;
        }
        return cartMapper.selectCartProductCheckedStatusByUserId(userId) == 0 ? true : false;
    }


}
