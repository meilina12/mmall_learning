package com.mmall.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.mmall.common.ServerResponse;
import com.mmall.dao.CategoryMapper;
import com.mmall.pojo.Category;
import com.mmall.service.ICategoryService;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Mei
 * @since 2019—01-16
 */
@Service("iCategoryService")
public class CategoryServiceImpl implements ICategoryService {

    private Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);

    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public ServerResponse addCategory(String name, Integer parentId) {
        if(parentId==null || StringUtils.isBlank(name)){
            return ServerResponse.createByERRORMessage("添加种类参数错误");
        }
        Category category=new Category();
        category.setParentId(parentId);
        category.setStatus(true);
        category.setName(name);
        int count = categoryMapper.insert(category);
        if(count > 0){
            return ServerResponse.createBySuccessMessage("增添种类成功！");
        }
        return ServerResponse.createByERRORMessage("增添种类失败");
    }

    public ServerResponse updateCategory(String categoryName,Integer categoryId){
        if(categoryId==null || StringUtils.isBlank(categoryName)){
            return ServerResponse.createByERRORMessage("更新种类参数错误");
        }
        Category category=new Category();
        category.setId(categoryId);
        category.setName(categoryName);
        int count = categoryMapper.updateByPrimaryKeySelective(category);
        if(count > 0){
            return ServerResponse.createBySuccessMessage("更新种类成功！");
        }
        return ServerResponse.createByERRORMessage("更新种类失败");
    }

    public ServerResponse<List<Category>> getChildParaller(Integer id){
        if(id==null){
            return ServerResponse.createByERRORMessage("参数错误");
        }
        List<Category> categories = categoryMapper.selectByChildParaller(id);
        if(CollectionUtils.isEmpty(categories)){
            logger.info("未找到当前id的子类");
        }
        return ServerResponse.createBySuccess(categories);
    }

    public ServerResponse<List<Integer>> getChildCategory(Integer categoryId) {
        if(categoryId==null){
            return ServerResponse.createByERRORMessage("参数错误");
        }
        Set<Category> categorySet=Sets.newHashSet();
        findChildCategory(categorySet,categoryId);
        //得到所有的id
        List<Integer> ids= Lists.newArrayList();
        for(Category category:categorySet){
            ids.add(category.getId());
        }
        return ServerResponse.createBySuccess(ids);
    }

    public void findChildCategory(Set<Category> categorySet,Integer categoryId){
        Category category = categoryMapper.selectByPrimaryKey(categoryId);
        if(category != null){
            categorySet.add(category);
        }
        List<Category> categories = categoryMapper.selectByChildParaller(categoryId);
        for(Category categoryItem:categories){
            findChildCategory(categorySet,categoryItem.getId());
        }
        return ;

    }



}
