package com.mmall.service.impl;

import com.google.common.collect.Lists;
import com.mmall.service.IFileService;
import com.mmall.util.FTPUtil;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Mei
 * @since 2019—01-24
 */
@Service("iFileService")
public class FileServiceImpl implements IFileService {

    private static final Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

    //将上传的文件的文件名返回去  参数 上传的文件名   文件路径
    public String upload(MultipartFile file, String path) {
        //文件名
        String filename = file.getOriginalFilename();
        //获取扩展名   从后面开始获取.
        String fileExtensionName = filename.substring(filename.lastIndexOf(".") + 1);
        //  上传文件的名字[若小A传了a.jpg文件，小B传了a.jpg，避免后面的文件覆盖前面的 因此需要独一无二的上传的文件名]
        String uploadFileName = UUID.randomUUID().toString() + "." + filename;
        logger.info("开始上传文件，上传文件的文件名:{},上传的路径:{},新文件名:{}", filename, path, uploadFileName);
        File fileDir = new File(path);
        if (!fileDir.exists()) {
            //有权限 可以写
            fileDir.setWritable(true);
            fileDir.mkdirs();
        }
        File targetFile = new File(path, uploadFileName);

        try {
            file.transferTo(targetFile);
            //以上上传文件成功  上传到tomcat服务器的webapp/upload中
            // 将targetFile上传到ftp服务器上
            FTPUtil.uploadFile(Lists.newArrayList(targetFile));
            // 上传完之后将upload文件夹删掉
            targetFile.delete();
        } catch (IOException e) {
            logger.error("上传文件异常", e);
            return null;
        }
        return targetFile.getName();
    }


}
