package com.mmall.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.mmall.common.Consts;
import com.mmall.common.Consts.ProductListOrderBy;
import com.mmall.common.Consts.ProductStatusEnum;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.dao.CategoryMapper;
import com.mmall.dao.ProductMapper;
import com.mmall.pojo.Category;
import com.mmall.pojo.Product;
import com.mmall.service.ICategoryService;
import com.mmall.service.IProductService;
import com.mmall.util.DateTimeUtil;
import com.mmall.util.PropertiesUtil;
import com.mmall.vo.ProductDetailVo;
import com.mmall.vo.ProductListVo;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Mei
 * @since 2019—01-18
 */
@Service("iProductService")
public class ProductServiceImpl implements IProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private ICategoryService categoryService;

    //添加商品 或者更新商品
    public ServerResponse saveOrUpdate(Product product) {
        if (product != null) {
            //将子图的第一张作为主图
            if (StringUtils.isNotBlank(product.getSubImages())) {
                String[] images = product.getSubImages().split(",");
                if (images.length > 0) {
                    product.setMainImage(images[0]);
                }
            }
            if (product.getId() != null) {//更新操作
                int count = productMapper.updateByPrimaryKeySelective(product);
                if (count > 0) {
                    return ServerResponse.createBySuccess("更新商品成功");
                }
                return ServerResponse.createByERRORMessage("更新商品失败");
            } else {//保存
                int count = productMapper.insert(product);
                if (count > 0) {
                    return ServerResponse.createBySuccess("添加商品成功");
                }
                return ServerResponse.createByERRORMessage("添加商品失败");
            }
        }
        return ServerResponse.createByERRORMessage("添加/更新商品 参数错误");
    }

    @Override
    public ServerResponse setProductStatus(Integer productId, Integer status) {
        if (productId == null || status == null) {
            return ServerResponse.createByERROR(ResponseCode.ILLEGAL_ARGUMENT.getCode(), "产品ID或者产品状态码有错");
        }
        int count = productMapper.updateProduceStatus(productId, status);
        if (count > 0) {
            return ServerResponse.createBySuccess("更新产品状态成功");
        }
        return ServerResponse.createByERRORMessage("更新产品状态失败");
    }

    @Override
    public ServerResponse<ProductDetailVo> manageProductDetail(Integer productId) {
        if (productId == null) {
            return ServerResponse.createByERRORMessage("产品参数为空");
        }
        Product product = productMapper.selectByPrimaryKey(productId);
        if (product == null) {
            return ServerResponse.createByERRORMessage("不存在该产品");
        }
        //assemble vo
        ProductDetailVo productDetailVo = assembleProductDetail(product);
        return ServerResponse.createBySuccess(productDetailVo);
    }

    @Override
    public ServerResponse<PageInfo> getProductList(Integer pageNum, Integer pageSize) {
        //1.startPage---start
        PageHelper.startPage(pageNum, pageSize);
        //2.sql逻辑
        List<Product> products = productMapper.selectList();
        List<ProductListVo> productListVoList = Lists.newArrayList();
        for (Product product : products) {
            ProductListVo productListVo = assembleProductListVo(product);
            productListVoList.add(productListVo);
        }
        //3.pageHelper---end
        PageInfo pageResult = new PageInfo(products);
        pageResult.setList(productListVoList);
        return ServerResponse.createBySuccess(pageResult);
    }

    @Override
    public ServerResponse<PageInfo> searchProduct(String productName, Integer productId, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        if (StringUtils.isNotBlank(productName)) {
            productName = new StringBuilder().append("%").append(productName).append("%").toString();
        }
        List<Product> products = productMapper.selectByNameId(productName, productId);
        List<ProductListVo> productListVoList = Lists.newArrayList();
        for (Product product : products) {
            ProductListVo productListVo = assembleProductListVo(product);
            productListVoList.add(productListVo);
        }
        PageInfo pageResult = new PageInfo(products);
        pageResult.setList(productListVoList);
        return ServerResponse.createBySuccess(pageResult);
    }

    public ProductListVo assembleProductListVo(Product product) {
        ProductListVo productListVo = new ProductListVo();
        productListVo.setId(product.getId());
        productListVo.setCategoryId(product.getCategoryId());
        productListVo.setName(product.getName());
        productListVo.setSubtitle(product.getSubtitle());
        productListVo.setMainImage(product.getMainImage());
        productListVo.setPrice(product.getPrice());
        productListVo.setStatus(product.getStatus());
        productListVo.setImageHost(PropertiesUtil.getProperty(Consts.ftp_server_http_prefix, Consts.ftp_server_http_prefix_defaultValue));
        return productListVo;
    }

    public ProductDetailVo assembleProductDetail(Product product) {
        ProductDetailVo productDetailVo = new ProductDetailVo();
        productDetailVo.setId(product.getId());
        productDetailVo.setName(product.getName());
        productDetailVo.setSubtitle(product.getSubtitle());
        productDetailVo.setCategoryId(product.getCategoryId());
        productDetailVo.setDetail(product.getDetail());
        productDetailVo.setMainImage(product.getMainImage());
        productDetailVo.setSubImages(product.getSubImages());
        productDetailVo.setStock(product.getStock());
        productDetailVo.setPrice(product.getPrice());
        productDetailVo.setStatus(product.getStatus());
        //ImageHost   从配置文件中读取  如果配置文件没有 则使用默认值
        productDetailVo.setImageHost(PropertiesUtil.getProperty(Consts.ftp_server_http_prefix, Consts.ftp_server_http_prefix_defaultValue));
        //ParentCategoryId
        Category category = categoryMapper.selectByPrimaryKey(product.getCategoryId());
        if (category == null) {//默认根节点
            productDetailVo.setParentCategoryId(0);
        } else {
            productDetailVo.setParentCategoryId(category.getParentId());
        }
        //CreateTime   UpdateTime   从数据库中取出来是时间戳  因此要进行处理
        productDetailVo.setCreateTime(DateTimeUtil.dateToStr(product.getCreateTime()));
        productDetailVo.setUpdateTime(DateTimeUtil.dateToStr(product.getUpdateTime()));
        return productDetailVo;
    }


    @Override
    public ServerResponse<PageInfo> getProductBykeywordCategory(String keyword, Integer category, Integer pageNum, Integer pageSize, String orderBy) {
        if (StringUtils.isBlank(keyword) && category == null) {
            return ServerResponse.createByERROR(ResponseCode.ILLEGAL_ARGUMENT.getCode(), ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        List<Integer> categoryIdList = Lists.newArrayList();
        //根据category查找
        if (category != null) {
            Category category1 = categoryMapper.selectByPrimaryKey(category);
            if (category1 == null && StringUtils.isEmpty(keyword)) {
                //不存在该分类 并且没有关键字 这个时候返回一个空的结果集
                PageHelper.startPage(pageNum, pageSize);
                List<ProductListVo> productListVoList = Lists.newArrayList();
                PageInfo pageInfo = new PageInfo(productListVoList);
                return ServerResponse.createBySuccess(pageInfo);
            }
            //根据种类查出所有的子类
            categoryIdList = categoryService.getChildCategory(category).getData();
        }
        if (StringUtils.isNotBlank(keyword)) {
            keyword = new StringBuilder().append("%").append(keyword).append("%").toString();
        }
        //动态排序
        PageHelper.startPage(pageNum, pageSize);
        if (StringUtils.isNotBlank(orderBy)) {
            if (ProductListOrderBy.PRICE_ASC_DESC.contains(orderBy)) {
                String[] order = orderBy.split("_");
                PageHelper.orderBy(order[0] + " " + order[1]);
            }
        }

        keyword = StringUtils.isBlank(keyword) ? null : keyword;
        System.out.println(keyword);
        categoryIdList = categoryIdList.size() == 0 ? null : categoryIdList;
        List<Product> list = productMapper.selectByNameAndCategoryIds(keyword, categoryIdList);
        List<ProductListVo> productListVoList = Lists.newArrayList();
        for (Product p : list) {
            productListVoList.add(assembleProductListVo(p));
        }
        PageInfo pageInfo = new PageInfo<>(list);
        pageInfo.setList(productListVoList);
        return ServerResponse.createBySuccess(pageInfo);

    }

    @Override
    public ServerResponse<ProductDetailVo> getProductDetail(Integer productId) {
        if (productId == null) {
            return ServerResponse.createByERRORMessage("产品参数为空");
        }
        Product product = productMapper.selectByPrimaryKey(productId);
        if (product == null) {
            return ServerResponse.createByERRORMessage("不存在该产品");
        }
        if (product.getStatus() != ProductStatusEnum.on_Sale.getCode()) {
            return ServerResponse.createByERRORMessage("该产品已经下架或者删除");
        }
        //assemble vo
        ProductDetailVo productDetailVo = assembleProductDetail(product);
        return ServerResponse.createBySuccess(productDetailVo);
    }

}
