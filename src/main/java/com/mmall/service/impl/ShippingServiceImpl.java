package com.mmall.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.dao.ShippingMapper;
import com.mmall.pojo.Shipping;
import com.mmall.service.IShippingService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Mei
 * @since 2019—01-31
 */
@Service("iShippingService")
public class ShippingServiceImpl implements IShippingService {

    @Autowired
    private ShippingMapper shippingMapper;

    public ServerResponse add(Integer userId, Shipping shipping) {
        //这里修改了insert方法  将id封装到shipping中
        shipping.setUserId(userId);
        int count = shippingMapper.insert(shipping);
        if (count > 0) {
            Map map = new HashMap();
            map.put("shippingId", shipping.getId());
            return ServerResponse.createBySuccess("添加地址成功", map);
        }
        return ServerResponse.createByERRORMessage("添加地址失败");
    }

    @Override
    public ServerResponse delete(Integer userId, Integer shippingId) {
        if (shippingId == null || shippingId < 0) {
            return ServerResponse.createByERROR(ResponseCode.ILLEGAL_ARGUMENT.getCode(), ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }

        //会有横向越权的问题  若登录之后 传一个shippingId 可以删除别人的
        int count = shippingMapper.deleteByUserIdAndShippingId(userId, shippingId);
        if (count > 0) {
            return ServerResponse.createBySuccess("地址删除成功");
        }
        return ServerResponse.createByERRORMessage("地址删除失败");
    }

    public ServerResponse update(Integer userId, Shipping shipping) {
        //这里修改了insert方法  将id封装到shipping中
        shipping.setUserId(userId);
        int count = shippingMapper.updateByshipping(shipping);
        if (count > 0) {
            return ServerResponse.createBySuccess("地址更新成功");
        }
        return ServerResponse.createByERRORMessage("地址更新失败");
    }

    @Override
    public ServerResponse<Shipping> select(Integer userId, Integer shippingId) {
        System.out.println(userId + "," + shippingId);
        Shipping shipping = shippingMapper.selectByUserIdShippingId(userId, shippingId);
        if (shipping == null) {
            return ServerResponse.createByERRORMessage("没有该收货地址");
        }
        return ServerResponse.createBySuccess("得到收货地址成功", shipping);
    }

    @Override
    public ServerResponse<PageInfo> list(Integer pageNum, Integer pageSize, Integer userId) {
        PageHelper.startPage(pageNum, pageSize);
        List<Shipping> shippings = shippingMapper.selectByUserId(userId);
        PageInfo pageInfo = new PageInfo(shippings);
        return ServerResponse.createBySuccess(pageInfo);
    }

}
