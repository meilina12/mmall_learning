package com.mmall.service.impl;

import com.mmall.common.Consts;
import com.mmall.common.Consts.Role;
import com.mmall.common.ResponseCode;
import com.mmall.common.TokenCache;
import com.mmall.common.ServerResponse;
import com.mmall.dao.UserMapper;
import com.mmall.pojo.User;
import com.mmall.service.IUserService;
import com.mmall.util.MD5Util;
import java.util.UUID;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Mei
 * @since 2019—01-14
 */
@Service("iUserService")
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public ServerResponse<User> login(String username, String password) {

        int count = userMapper.checkUsername(username);
        if(count == 0){
            return ServerResponse.createByERRORMessage("用户名不存在");
        }
        // todo MD5加密
        String Md5Password = MD5Util.MD5EncodeUtf8(password);
        User user = userMapper.selectLogin(username, Md5Password);
        if(null == user){
            return ServerResponse.createByERRORMessage("密码错误");
        }

        user.setPassword(StringUtils.EMPTY);
        return ServerResponse.createBySuccess("登陆成功",user);
    }

    @Override
    public ServerResponse<String> register(User user) {
        //检验用户名 邮箱
        ServerResponse<String> response;
        response = checkValid(user.getUsername(),Consts.USERNAME);
        if(!response.isSuccess()){
            return response;
        }
        response = checkValid(user.getEmail(),Consts.EMAIL);
        if(!response.isSuccess()){
            return response;
        }
        //新增数据之前 应该将密码进行加密
        String Md5Password = MD5Util.MD5EncodeUtf8(user.getPassword());
        user.setPassword(Md5Password);
        //用户设置权限
        user.setRole(Role.ROLE_CUSTOMER);

        int insert = userMapper.insert(user);
        if(insert == 0){
            return ServerResponse.createByERRORMessage("注册失败");
        }
        return ServerResponse.createBySuccessMessage("注册成功");

    }

    @Override
    public ServerResponse<String> checkValid(String str,String type){
        if(StringUtils.isNotBlank(type)){
            int count;
            if(Consts.USERNAME.equals(type)){
                count = userMapper.checkUsername(str);
                if(count > 0){
                    return ServerResponse.createByERRORMessage("用户名已存在");
                }
            }
            if(Consts.EMAIL.equals(type)){
                count=userMapper.checkEmail(str);
                if(count > 0){
                    return ServerResponse.createByERRORMessage("邮箱已存在");
                }
            }
            return ServerResponse.createBySuccessMessage("校验成功");
        }
        return ServerResponse.createByERRORMessage("参数错误");
    }

    @Override
    public ServerResponse<String> forgetGetQuestion(String username) {
        ServerResponse<String> validResponse = checkValid(username, Consts.USERNAME);
        if(validResponse.isSuccess()){
            //用户不存在
            return ServerResponse.createByERRORMessage("用户不存在");
        }
        //根据用户名查答案
        String question = userMapper.getQuestionByusername(username);
        if(StringUtils.isBlank(question)){
            return ServerResponse.createByERRORMessage("找回密码的问题为空");
        }
        return ServerResponse.createBySuccess(question);
    }

    @Override
    public ServerResponse<String> checkAnswer(String username,String question,String answer) {
        ServerResponse<String> valid = checkValid(username, Consts.USERNAME);
        if(valid.isSuccess()){
            return ServerResponse.createByERRORMessage("用户名不存在");
        }
        int count = userMapper.checkAnswer(username, question,answer);
        if(count > 0){
            //得到UUID
            String forgetUUID= UUID.randomUUID().toString();
            //将UUID放到本地缓存里  这里使用guava
            TokenCache.setKey(Consts.TOKEN_PREFIX+username,forgetUUID);
            return ServerResponse.createBySuccess(forgetUUID);
        }
        return ServerResponse.createByERRORMessage("答案错误");
    }

    @Override
    public ServerResponse<String> forgetSetNewPwd(String username, String newPwd, String forgetUUID) {
        if(StringUtils.isBlank(forgetUUID)){
            return ServerResponse.createByERRORMessage("参数错误，forgetUUID为空");
        }
        if(checkValid(username,Consts.USERNAME).isSuccess()){//用户名不存在
            return ServerResponse.createByERRORMessage("用户名不存在");
        }
        //获得缓存中的UUID
        String token = TokenCache.getKey(Consts.TOKEN_PREFIX + username);
        if(StringUtils.isBlank(token)){
            return ServerResponse.createByERRORMessage("token不存在或者token失效过期");
        }
        if(StringUtils.equals(forgetUUID,token)){
            //将密码进行加密
            String Md5Pwd=MD5Util.MD5EncodeUtf8(newPwd);
            //重置密码
            int count = userMapper.setPwdByusername(username, Md5Pwd);
            if(count > 0){
                return ServerResponse.createBySuccessMessage("重置密码成功");
            }
        }else{
            return ServerResponse.createByERRORMessage("token错误，请重新获取重置密码的token");
        }
        return ServerResponse.createByERRORMessage("重置密码失败");
    }

    @Override
    public ServerResponse<String> resetPassword(User user, String oldPwd, String newPwd) {
        int count = userMapper.checkPassword(user.getId(), MD5Util.MD5EncodeUtf8(user.getPassword()));
        if(count == 0){
            return ServerResponse.createByERRORMessage("密码错误");
        }
        user.setPassword(MD5Util.MD5EncodeUtf8(newPwd));
        count = userMapper.updateByPrimaryKeySelective(user);
        if(count > 0){
            return ServerResponse.createByERRORMessage("密码重置成功");
        }
        return ServerResponse.createByERRORMessage("密码重置失败");
    }

    @Override
    public ServerResponse<User> updateUserInfo(User user) {
        //检验email
        int count = userMapper.checkEmailByUseId(user.getId(), user.getEmail());
        if(count > 0){
            return ServerResponse.createByERRORMessage("该email已存在 请更换");
        }
        User updateUser=new User();
        updateUser.setId(user.getId());
        updateUser.setUsername(user.getUsername());
        updateUser.setEmail(user.getEmail());
        updateUser.setPhone(user.getPhone());
        updateUser.setQuestion(user.getQuestion());
        updateUser.setAnswer(user.getAnswer());
        count = userMapper.updateByPrimaryKeySelective(updateUser);
        if(count > 0){
            return ServerResponse.createBySuccess("信息更新成功",updateUser);
        }
        return ServerResponse.createByERRORMessage("信息更新失败");
    }

    @Override
    public ServerResponse<User> getInformation(Integer id) {
        User user = userMapper.selectByPrimaryKey(id);
        if(user==null){
            return ServerResponse.createBySuccessMessage("找不到该用户");
        }
        user.setPassword(StringUtils.EMPTY);
        return ServerResponse.createBySuccess(user);
    }


    @Override
    public ServerResponse checkAdmin(User user) {
        if(user!=null && user.getRole().intValue() == Role.ROLE_ADMIN){
            return ServerResponse.createBySuccess();
        }
        return ServerResponse.createByERROR();
    }

    @Override
    public ServerResponse checkLoginAdmin(HttpSession session) {
        User user = (User) session.getAttribute(Consts.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByERROR(ResponseCode.NEED_LOGIN.getCode(), "用户未登录");
        }
        if (checkAdmin(user).isSuccess()) {
            return ServerResponse.createBySuccessMessage("登录用户是管理员");
        }
        return ServerResponse.createByERRORMessage("无权限操作，需要管理员操作");
    }

}
