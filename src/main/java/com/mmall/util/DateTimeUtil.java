package com.mmall.util;

import java.util.Date;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * @author Mei
 * @since 2019—01-22
 */
public class DateTimeUtil {

    private static final String STAND_PATTERN = "yyyy-MM-dd HH:mm:ss";


    //strToDate
    public static Date strToDate(String str, String pattern) {
        DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern(pattern);
        DateTime dateTime = dateTimeFormat.parseDateTime(str);
        return dateTime.toDate();
    }

    public static String dateToStr(Date date, String pattern) {
        if (date == null) {
            return StringUtils.EMPTY;
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(pattern);
    }

    public static Date strToDate(String str) {
        DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern(STAND_PATTERN);
        DateTime dateTime = dateTimeFormat.parseDateTime(str);
        return dateTime.toDate();
    }

    public static String dateToStr(Date date) {
        if (date == null) {
            return StringUtils.EMPTY;
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(STAND_PATTERN);
    }

}
