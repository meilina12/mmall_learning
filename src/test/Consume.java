/**
 * @author Mei
 * @since 2019—01-14
 */
public class Consume extends Thread {

    private Producer producer;

    public Consume(String name, Producer producer) {
        super(name);
        this.producer = producer;
        System.out.println("Thread " + name + " created");
    }

    @Override
    public void run() {
        while (true) {
            String msg = producer.delMsgs();
            System.out.println(Thread.currentThread().getName() + " consume " + msg);
        }
    }

    public static void main(String[] args) {
        Producer producer = new Producer();
        Consume consume1 = new Consume("c1", producer);
        Consume consume2 = new Consume("c2", producer);
        Consume consume3 = new Consume("c3", producer);
        producer.start();
        consume1.start();
        consume2.start();
        consume3.start();
    }

}
