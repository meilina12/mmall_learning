import java.util.ArrayList;

/**
 * @author Mei
 * @since 2019—01-14
 */
public class Producer extends Thread {

    public  ArrayList<String> Msgs=new ArrayList<>();


    @Override
    public void run() {
        while(true){
            addMag();
        }
    }

    public void addMag(){
        synchronized (Msgs){
            System.out.println("produce is running");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Msgs.add("hahah");
            System.out.println("produce a Msg...");
            Msgs.notify();
//            Msgs.notifyAll();
        }
    }

    public String delMsgs(){
        synchronized (Msgs){
            System.out.println(Thread.currentThread().getName()+" is running");
            if(Msgs.size()==0){
                try {
                    System.out.println(Thread.currentThread().getName()+" is waiting...");
                    Msgs.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return Msgs.remove(0);
        }
    }





}
