/**
 * @author Mei
 * @since 2019—01-14
 */
public class ThreadLocalTest  implements Runnable{

    ThreadLocal<Integer> threadLocal=new ThreadLocal<>();
    @Override
    public void run() {
        threadLocal.set((int) (Math.random()*100));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(threadLocal.get());
    }




    public static void main(String[] args){
        ThreadLocalTest threadLocalTest=new ThreadLocalTest();
        Thread thread=new Thread(threadLocalTest);
        Thread thread1=new Thread(threadLocalTest);
        thread.start();
        thread1.start();
        try {
            thread.join();
            thread1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
